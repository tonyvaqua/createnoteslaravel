<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use Auth;

class NoteController extends Controller
{
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::where('access_type', 1)->orWhere('user_id', Auth::user()->id)->get();

        return view('pages.notes.index')->with('notes', $notes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.notes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        try{
            $note = new Note();
            $note->note_title = $input['title'];
            $note->note = $input['note'];
            $note->user_id = Auth::user()->id;
            $note->access_type = 0;
            $note->slug = unique_random('notes', 'slug', 10);
            $note->save();
        }
        catch (QueryException $e) {
            Log::info($e);
            return redirect()->back()->with('error', 'Unexpected Error!');
        }

        return redirect()->back()->with('success', 'Note Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $note = Note::where('slug', $slug)->first();
        return view('pages.notes.show')->with('note', $note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, $status)
    {
        $affectedRows = 0;

        try{
            $affectedRows = Note::where('id', $id)
                ->where('user_id', Auth::user()->id)
                ->update(['access_type' => $status]);
        }
        catch (QueryException $e) {
            Log::info($e);
            return redirect()->back()->with('error', 'Unexpected Error!');
        }

        if ($affectedRows === 1)
            return redirect()->back()->with('success', 'Status Changed Successfully');
        else
            return redirect()->back()->with('error', 'Updation Failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
