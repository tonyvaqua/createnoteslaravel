<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->engine = 'INNODB';

            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('note_title');
            $table->text('note');
            $table->boolean('access_type')->default(0)->comment("1 for public, 0 for private");
            $table->string('slug')->unique()->comment("for unique url");
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notes');
    }
}
