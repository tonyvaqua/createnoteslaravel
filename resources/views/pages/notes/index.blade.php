@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Notes</div>

                    <div class="panel-body">
                        @include('includes.errors')

                            <div class="bs-example" data-example-id="hoverable-table">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th style="width: 400px;">Note</th>
                                        <th>Status</th>
                                        <th>User</th>
                                        <th>Created At</th>
                                        <th>View</th>
                                        <th>Change Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($notes as $note)

                                        <tr>
                                            <td>{{$note->note_title}}</td>
                                            <td>{{substr($note->note, 0, 100)}}....</td>
                                            <td>
                                                {{$note->access_type == '1' ? 'Public' : 'Private'}}
                                            </td>
                                            <td>{{$note->user->name}}</td>
                                            <td>{{date('d-M-Y', strtotime($note->created_at))}}</td>
                                            <td> <a target="_blank" href="{{route('showNote', $note->slug)}}">View</a></td>
                                            <td>
                                                <a href="{{route('changeStatus', [$note->id, $note->access_type == '1' ? '0' : '1'])}}">
                                                    {{$note->access_type == '1' ? 'Make Private' : 'Make Public'}}
                                                </a>
                                            </td>
                                        </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
