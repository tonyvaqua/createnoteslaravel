@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Note</div>

                    <div class="panel-body">

                        <div class="panel">
                            <div class="panel-heading">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <h3 class="pull-left">{{$note->note_title}}</h3>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="pull-right">
                                                <small><em>{{date('d-M-Y', strtotime($note->created_at))}}</em></small>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body">
                                {{$note->note}}
                            </div>

                            <div class="panel-footer">
                                Comments
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

