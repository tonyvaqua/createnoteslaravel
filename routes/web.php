<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/note/add', 'NoteController@create')->name('addNote');
    Route::post('/note/add', 'NoteController@store');
    Route::get('/note/list', 'NoteController@index')->name('listNotes');
    Route::get('/view-note/{slug}', 'NoteController@show')->name('showNote');
    Route::get('/change-status/{id}/{status}', 'NoteController@update')->name('changeStatus');
});


